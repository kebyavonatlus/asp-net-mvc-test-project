﻿using Microsoft.EntityFrameworkCore;
using TestMVC.Models;

namespace TestMVC.Context
{
    public class TestMVCContext : DbContext
    {
        public TestMVCContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Site> Sites { get; set; }
    }
}
