﻿namespace TestMVC.Models.VM
{
    public class SiteCreateModel
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public bool IsSecure { get; set; }
    }
}
