﻿namespace TestMVC.Models.VM
{
    public class SiteEditModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public bool IsSecure { get; set; }
    }
}
