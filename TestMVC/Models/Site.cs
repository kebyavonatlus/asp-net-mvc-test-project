﻿namespace TestMVC.Models
{
    public class Site
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public bool IsSecure { get; set; }
    }
}
