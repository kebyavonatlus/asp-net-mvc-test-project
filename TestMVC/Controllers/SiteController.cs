﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using TestMVC.Context;
using TestMVC.Models;
using TestMVC.Models.VM;

namespace TestMVC.Controllers
{
    public class SiteController : Controller
    {
        private readonly TestMVCContext _context;
        public SiteController(TestMVCContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult All()
        {
            var sites = _context.Sites.ToList();
            return View(sites);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(SiteCreateModel siteCreateModel)
        {
            var site = new Site
            {
                IsSecure = siteCreateModel.IsSecure,
                Name = siteCreateModel.Name,
                Url = siteCreateModel.Url
            };

            _context.Sites.Add(site);
            _context.SaveChanges();

            TempData["success"] = $"Сайт {site.Name} успешно создан с ID {site.Id}";

            dynamic obj;
            obj = 1;
            obj = "1";
            return RedirectToAction("All");
        }

        [HttpGet]
        public IActionResult Edit(int Id)
        {
            var model = _context.Sites.FirstOrDefault(x => x.Id == Id);

            if (model == null)
                return RedirectToAction("All");

            var viewModel = new SiteEditModel
            {
                Id = model.Id,
                IsSecure = model.IsSecure,
                Url = model.Url,
                Name = model.Name
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Edit(SiteEditModel editModel)
        {
            var model = _context.Sites.FirstOrDefault(x => x.Id == editModel.Id);

            if (model == null)
                return View(model);

            model.IsSecure = editModel.IsSecure;
            model.Name = editModel.Name;
            model.Url = editModel.Url;

            _context.Sites.Update(model);
            _context.SaveChanges();

            TempData["success"] = $"Сайт {model.Name} успешно изменен.";

            return RedirectToAction("All");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            var site = _context.Sites.FirstOrDefault(x => x.Id == id);

            if (site == null)
            {
                TempData["error"] = $"Не удалось найти объект с ID: {id}";
                return RedirectToAction("All");
            }

            _context.Sites.Remove(site);
            _context.SaveChanges();

            TempData["success"] = $"Сайт {site.Name} успешно удален.";

            return RedirectToAction("All");
        }
    }
}
